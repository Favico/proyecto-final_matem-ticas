import  pygame, sys
from pygame.locals import *
import Funciones as fun
from configuraciones import Configuraciones
import Actividad

NEGRO = (0, 0, 0)

def main():
    pygame.init()
    pygame.display.set_caption("Aprendiendo Matemáticas")
    #-------------------Resolución de Pantalla-------------------------------------------------------------------------
    configuraciones = Configuraciones()
    Tamaño = (configuraciones.screen_width, configuraciones.screen_heigt)
    pantalla = pygame.display.set_mode(Tamaño)
    #-------------------------------Imagnes Principales----------------------------------------------------------------
    reloj = pygame.time.Clock()

    imagen_fondo = pygame.image.load("imagenes/portada.jpg").convert()
    posicon_inicio = [0, 0]
    # ----------------------------------------Imagenes de Botones---------------------------------------------
    img = pygame.image.load("imagenes/jugar.png")
    img2 = pygame.image.load("imagenes/jugar2.png")
    boton = fun.Boton(img, img2, 325, 350)
    cursor = fun.Cursor()
    #Cargamos Imagen de Icono
    icon = pygame.image.load("imagenes/icon.png")
    pygame.display.set_icon(icon)



    #---------------------Bucle que hace que el programa se mantenga abierto.-------------------------------
    Cerrar = False
    while not Cerrar:

        reloj.tick(10)
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                Cerrar = True
            if evento.type == pygame.MOUSEBUTTONDOWN:
                if cursor.colliderect(boton.rect):
                    pygame.display.set_mode(Actividad.inicio())



        #Carga la imagen en pantalla

        Fuente = pygame.font.Font("Fuentes/AliceandtheWickedMonster.ttf", 20)
        Texto = Fuente.render("Version 1.1.1", True, NEGRO)
        pantalla.blit(imagen_fondo, posicon_inicio, )
        pantalla.blit(Texto, [450, 575])
        cursor.update()
        boton.update(pantalla, cursor)
        pygame.display.flip()
        reloj.tick(60)


    pygame.quit()

if __name__ == "__main__":
    main()