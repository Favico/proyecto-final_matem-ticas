import pygame


def inicio():
    pygame.init()
    pantallaActividad =pygame.display.set_mode([900, 600])
    pygame.display.set_caption("Actividad 1")
    reloj = pygame.time.Clock()
    posicion= [0, 0]
    fondo = pygame.image.load("imagenes/fondo2.jpg").convert()

    findeljuego = False
    while not findeljuego:
        reloj.tick(10)
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                findeljuego = True

        pantallaActividad.blit(fondo, posicion,)
        pygame.display.flip()
        reloj.tick(60)
    pygame.quit()
if __name__ == "__main__":
    inicio()